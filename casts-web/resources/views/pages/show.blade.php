@extends('layouts.master')

@section('title', 'Cast Detail Page')

@section('content')

    <form>
        <div class="form-group">
            <label for="updateName">Name</label>
            <input type="text" class="form-control" id="updateName" value="{{ $cast->name }}">
        </div>
        <div class="form-group">
            <label for="updateAge">Age</label>
            <input type="number" class="form-control" id="updateAge" value="{{ $cast->age }}">
        </div>
        <div class="form-group">
            <label for="createBio">Bio</label>
            <textarea class="form-control" id="createBio" rows="10">{{ $cast->bio }}</textarea>
        </div>
        <a href="{{ route('cast.index') }}" class="btn btn-secondary">Back to List Cast</a>
    </form>

@endsection

@push('scripts')
    <script src="{{ asset('/templates/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/templates/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush
