@extends('layouts.master')

@section('title', 'Cast Data Page')

@section('content')

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cast as $key => $value)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $value->name }}</td>
            <td>
                <a href="/cast/{{ $value->id }}" class="btn btn-primary"><i class="far fa-eye"></i></a>
                <a href="/cast/{{ $value->id }}/edit" class="btn btn-success"><i class="fas fa-edit"></i></a>
                <div class="btn-group" role="group">
                    <form id="deleteForm{{ $value->id }}" action="/cast/{{ $value->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="button" onclick="deleteCast('{{ $value->id }}')" class="btn btn-danger">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </form>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection()

@push('scripts')
<script src="{{ asset('/templates/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/templates/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });

    function deleteCast(id) {
        if (confirm('Are you sure you want to delete this cast?')) {
            document.getElementById('deleteForm' + id).submit();
        }
    }
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush