<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = DB::table('casts')->get();
        return view('pages.index', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'age' => 'required|numeric',
            'bio' => 'required',
        ]);

        $currentTime = now(); // Get the current time

        DB::table('casts')->insert([
            'name' => $request["name"],
            'age' => $request["age"],
            'bio' => $request["bio"],
            'created_at' => $currentTime,
            'updated_at' => $currentTime,
        ]);

        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('pages.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();

        if (!$cast) {
            return redirect('/cast')->with('error', 'Cast not found.');
        }

        return view('pages.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'age' => 'required|numeric',
            'bio' => 'required',
        ]);

        DB::table('casts')
            ->where('id', $id)
            ->update([
                'name' => $request["name"],
                'age' => $request["age"],
                'bio' => $request["bio"],
            ]);
        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storbio.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
